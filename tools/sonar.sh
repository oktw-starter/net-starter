#!/bin/bash

dotnet sonarscanner begin /k:"net-starter" /d:sonar.host.url="http://localhost:9000"  /d:sonar.login="sqp_de224e3c8496f9bfa54b78dec7efe9d13a86f5d2" /d:sonar.cs.vscoveragexml.reportsPaths=coverage.xml
dotnet build
dotnet-coverage collect 'dotnet test' -f xml  -o 'coverage.xml'
dotnet sonarscanner end /d:sonar.login="sqp_de224e3c8496f9bfa54b78dec7efe9d13a86f5d2"