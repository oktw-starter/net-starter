#!/bin/bash
echo "START plantuml documentation"

git config --global user.email $CI_GITLAB_USER
git config --global user.password $CI_GITLAB_TOKEN


echo "Generating documentation ..."
dotnet tool install --global PlantUmlClassDiagramGenerator
puml-gen ./pizzafactory-api/ ./pizzafactory-api/docs/ -dir -createAssociation -allInOne -excludePaths ./pizzafactory-api/bin,./pizzafactory-api/obj,./pizzafactory-api/Properties

echo "Add to README ..."
export PLANTUML_DIAGRAMME=$(cat ./pizzafactory-api/docs/include.puml)
envsubst '${PLANTUML_DIAGRAMME}' < ./pizzafactory-api/docs/README.md.tpl > ./README.md

git add ./README.md   
git add ./pizzafactory-api/docs/diagram.puml
git commit -m "[ci skip] Generation plantuml docs"
git remote set-url origin "https://$CI_GITLAB_USER:$CI_GITLAB_TOKEN@gitlab.com/$CI_PROJECT_PATH.git"
git push origin HEAD:$CI_COMMIT_REF_NAME

echo "END plantuml documentation"
