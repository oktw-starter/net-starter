namespace pizzafactory_test.Domain;
using pizzafactory_api.Domain;

[TestClass]
public class CookedPizzaTest
{

    [TestMethod]
    public void TestNewPizzaWithPizzaAndPizzaiolo_Success()
    {
        //arrange
        var pizza = new Pizza("PIZ-1",new List<string>().Append("bacon"));
        var pizzaiolo = new Pizzaiolo("Rafaelo");
        //act
        var cookedPizza = new CookedPizza(pizza,pizzaiolo);
        //assert
        Assert.AreEqual(pizza, cookedPizza.pizza);
        Assert.AreEqual(pizzaiolo, cookedPizza.pizzaiolo);
        Assert.IsNotNull(cookedPizza.cookDateTime);
        Assert.IsNotNull(cookedPizza.reference);
    }
}