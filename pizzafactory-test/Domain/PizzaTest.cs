using pizzafactory_api.Domain;

namespace pizzafactory_test.Domain;

[TestClass]
public class PizzaTest
{
    [TestMethod]
    public void TestNewPizzaWithoutLabel_ThowsArgumentException()
    {
             //arrange
        string inputEmptyLabel = "";
        IEnumerable<string> inputAdditionnalIngredients = new List<string>();
        inputAdditionnalIngredients.Append("bacon");
        //act
        var exception = Assert.ThrowsException<ArgumentException>(
            () => new Pizza(inputEmptyLabel,inputAdditionnalIngredients),
            "This should have thrown!");
        //assert
         Assert.AreEqual("The pizza label can't be null or empty.", exception.Message);
    }

    [TestMethod]
    public void TestNewPizzaWithLabel_Success()
    {
        //arrange
        string inputLabel = "PIZ-1";
        IEnumerable<string> inputAdditionnalIngredients = new List<string>();
        inputAdditionnalIngredients.Append("bacon");
        //act
        var pizza = new Pizza(inputLabel,inputAdditionnalIngredients);
        //assert
        Assert.AreEqual(inputLabel, pizza.label);
        Assert.AreEqual(inputAdditionnalIngredients, pizza.additionalIngredients);
    }
}