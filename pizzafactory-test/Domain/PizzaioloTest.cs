using System.Collections;
using pizzafactory_api.Domain;

namespace pizzafactory_test.Domain;

[TestClass]
public class PizzaioloTest
{
    [TestMethod]
    public void TestNewPizzaiolo_ThowsArgumentException()
    {
             //arrange
        string inputEmptyMatricule = "";
        //act
        var exception = Assert.ThrowsException<ArgumentException>(
            () => new Pizzaiolo(inputEmptyMatricule),
            "This should have thrown!");
        //assert
         Assert.AreEqual("The pizzaiolo Matricule can't be null or empty.", exception.Message);
    }

    [TestMethod]
    public void TestNewPizzaioloWithMatricule_Success()
    {
         //arrange
        string inputMatricule = "Michelangelo";
        //act
        var pizzaiolo = new Pizzaiolo(inputMatricule);
        //assert
        Assert.AreEqual(inputMatricule, pizzaiolo.matricule);
    }

    [TestMethod]
    public void TestReadBook_Success()
    {
         //arrange
         string inputMatricule = "Michelangelo";
        SortedList contentCookBook = new SortedList();
        contentCookBook.Add("PIZ-1", "Calzone");
        contentCookBook.Add("PIZ-2", "Napoletana");
        contentCookBook.Add("PIZ-3", "Romana");
        contentCookBook.Add("PIZ-4", "Regina");
        contentCookBook.Add("PIZ-5", "Quattro formaggi");
        //act
        var pizzaiolo = new Pizzaiolo(inputMatricule);
        
        //assert
        Assert.AreEqual(contentCookBook.Count, pizzaiolo.cookBook.Count);
    }
}