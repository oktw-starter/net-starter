using Microsoft.OpenApi.Models;
using pizzafactory_api.Infrastructure;
using pizzafactory_api.Application;
using System.Reflection;

internal class Program
{
    private static void Main(string[] args)
    {
        var builder = WebApplication.CreateBuilder(args);

        // Add services to the container.
        builder.Services.AddControllers()
            .AddJsonOptions(
                options => options.JsonSerializerOptions.PropertyNamingPolicy = null);

        // Add services to the container.
        builder.Services.Configure<PizzaFactoryStoreDatabaseSettings>(
            builder.Configuration.GetSection("PizzaFactoryStoreDatabase"));  
        builder.Services.AddSingleton<CookedPizzaMongoDBRepository>();
        builder.Services.AddTransient<PizzaService>();

        // Learn more about configuring Swagger/OpenAPI at https://aka.ms/aspnetcore/swashbuckle
        builder.Services.AddEndpointsApiExplorer();
        builder.Services.AddSwaggerGen(options =>
        {
            options.SwaggerDoc("v1", new OpenApiInfo
            {
                Version = "v1",
                Title = "Pizza Factory API",
                Description = "My pizza factory starter project",
                TermsOfService = new Uri("https://ninja-turtles.com/terms"),
                Contact = new OpenApiContact
                {
                    Name = "Michelangelo",
                    Url = new Uri("ttps://ninja-turtles.com/contact")
                },
                License = new OpenApiLicense
                {
                    Name = "Splinter License",
                    Url = new Uri("ttps://ninja-turtles.com/license")
                }
            });
       
            // using System.Reflection;
            var xmlFilename = $"{Assembly.GetExecutingAssembly().GetName().Name}.xml";
            options.IncludeXmlComments(Path.Combine(AppContext.BaseDirectory, xmlFilename));
        });

        var app = builder.Build();

        // // Configure the HTTP request pipeline.
        // Enable middleware to serve generated Swagger as a JSON endpoint.
        app.UseSwagger();
        // Enable middleware to serve swagger-ui (HTML, JS, CSS, etc.),
        // specifying the Swagger JSON endpoint.
        app.UseSwaggerUI(options =>
        {
            options.SwaggerEndpoint("/swagger/v1/swagger.json", "v1");
        });
        
        app.UseAuthorization();
        app.UseCors(builder => builder
            .AllowAnyOrigin()
            .AllowAnyMethod()
            .AllowAnyHeader()
        );   
        app.MapControllers();

        app.Run();
    }
}