using Microsoft.AspNetCore.Mvc;
using pizzafactory_api.Domain;
using pizzafactory_api.Application;
using System.ComponentModel.DataAnnotations;

namespace pizzafactory_api.Controllers;

[ApiController]
[Route("api/v1/[controller]")]
public class PizzasController : ControllerBase
{
    private readonly PizzaService _pizzaService;

    public PizzasController(PizzaService pizzaService) =>
        _pizzaService = pizzaService ?? throw new ArgumentNullException(nameof(pizzaService));

    /// <response code="201">Returns the newly created item</response>
    /// <response code="404">If the item is null</response>
    [HttpGet]
    [Produces("application/json")]
    [ProducesResponseType(typeof(CookedPizza), StatusCodes.Status200OK)]
    [ProducesResponseType(StatusCodes.Status404NotFound)]
    public async Task<ActionResult<List<CookedPizza>>> Get()
    {
        var cookedPizzas = await _pizzaService.GetCookedPizzas();
        if (cookedPizzas == null)
        {
            return NotFound();
        }
        return cookedPizzas;
    }


    /// <response code="201">Returns the newly created item</response>
    [HttpPost]
    [Produces("application/json")]
    [ProducesResponseType(typeof(CookedPizza), StatusCodes.Status201Created)]
    public IActionResult Post([FromBody] Pizza pizza)
    {
        var cookedPizza = _pizzaService.CookOnePizza(pizza);

        return CreatedAtAction(nameof(Post), cookedPizza);
    }
}