namespace pizzafactory_api.Domain;

public interface ICookedPizzaRepository
{
    public CookedPizza SaveOneCookedPizza(CookedPizza pizza);
    public Task<List<CookedPizza>> GetCookedPizzas();
}