namespace pizzafactory_api.Domain;

using System.Text.Json.Serialization;
using MongoDB.Bson;
using MongoDB.Bson.Serialization.Attributes;

public class CookedPizza
{
    [BsonId]
    [BsonRepresentation(BsonType.ObjectId)]
    [JsonInclude]
    public string reference;
    [JsonInclude]
    public Pizza pizza;
    [JsonInclude]
    public DateTime cookDateTime;
    [JsonInclude]
    public Pizzaiolo pizzaiolo;
    
    [JsonConstructor]
    public CookedPizza(Pizza pizza, Pizzaiolo pizzaiolo)
    {
        if (pizza == null)
            throw new InvalidDataException("The pizza to cook should be unasigned");
    
        if (pizzaiolo == null)
            throw new InvalidDataException("The pizzaiolo should be unasigned");

        if (!pizzaiolo.cookBook.ContainsKey(pizza.label))
        {
            throw new InvalidDataException("The pizza label isn't in the cookbook.");
        }

        this.reference = MongoDB.Bson.ObjectId.GenerateNewId().ToString();;
        this.pizzaiolo = pizzaiolo;
        this.pizza = pizza;
        var index = pizzaiolo.cookBook.IndexOfKey(pizza.label);
        this.pizza.name = (string)pizzaiolo.cookBook.GetByIndex(index);
        this.cookDateTime = DateTime.Now;
    }

    public static Task<List<CookedPizza>> GetCookedPizzas(ICookedPizzaRepository repository)
    {
        return repository.GetCookedPizzas();
    }
}
