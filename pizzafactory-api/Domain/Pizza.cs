using System.ComponentModel.DataAnnotations;
using System.Text.Json.Serialization;

namespace pizzafactory_api.Domain;

public class Pizza
{
    [Required]
    [JsonInclude]
    public string label;
    [JsonInclude]
    public string? name;
    [JsonInclude]
    public IEnumerable<string>? additionalIngredients;

    [JsonConstructor]
    public Pizza(string label, IEnumerable<string>? additionalIngredients)
    {
        if (String.IsNullOrEmpty(label))
            throw new ArgumentException("The pizza label can't be null or empty.");
       
        this.label = label;
        this.additionalIngredients = additionalIngredients;
    }

    public CookedPizza Cook(ICookedPizzaRepository repository)
    {
        if (this == null)
            throw new ArgumentException("Instance un pizza before cooking.");

        if (repository == null)
            throw new SystemException("The pizza repository can't be null or empty.");
        var pizzaiolo = new Pizzaiolo("donatello");
        var CookedPizza = new CookedPizza(this, pizzaiolo);

        return repository.SaveOneCookedPizza(CookedPizza);
    }
}
