namespace pizzafactory_api.Domain;

using System;
using System.Collections;
using System.Text.Json.Serialization;
using MongoDB.Bson;
using MongoDB.Bson.Serialization.Attributes;

public class Pizzaiolo
{
    [JsonInclude]
    public string matricule;
    public SortedList cookBook;

    public Pizzaiolo(string pizzaioloMatricule)
    {
        if (String.IsNullOrEmpty(pizzaioloMatricule))
            throw new ArgumentException("The pizzaiolo Matricule can't be null or empty.");

        matricule = pizzaioloMatricule;
        cookBook = ReadCookBook();
    }

    public SortedList ReadCookBook()
    {
        SortedList readedCookBook = new SortedList();
        readedCookBook.Add("PIZ-1", "Calzone");
        readedCookBook.Add("PIZ-2", "Napoletana");
        readedCookBook.Add("PIZ-3", "Romana");
        readedCookBook.Add("PIZ-4", "Regina");
        readedCookBook.Add("PIZ-5", "Quattro formaggi");

        return readedCookBook;
    }

}