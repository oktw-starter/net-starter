namespace pizzafactory_api.Application;

using pizzafactory_api.Domain;
using pizzafactory_api.Infrastructure;
using Microsoft.Extensions.Options;

public class PizzaService
{
    private ICookedPizzaRepository _cookedPizzaMongoDBRepository;

    public PizzaService(IOptions<PizzaFactoryStoreDatabaseSettings> pizzaFactoryStoreDatabaseSettings) => 
        _cookedPizzaMongoDBRepository = new CookedPizzaMongoDBRepository(pizzaFactoryStoreDatabaseSettings);

    public CookedPizza CookOnePizza(Pizza pizza)
    {
        var pizza2Cook = new Pizza(
            pizza.label,
            pizza.additionalIngredients
        );
        
        return pizza.Cook(_cookedPizzaMongoDBRepository);
    }

    public Task<List<CookedPizza>> GetCookedPizzas()
    {
        return CookedPizza.GetCookedPizzas(_cookedPizzaMongoDBRepository);

    }
}