namespace pizzafactory_api.Infrastructure;

public class PizzaFactoryStoreDatabaseSettings
{
    public string ConnectionString { get; set; } = null!;

    public string DatabaseName { get; set; } = null!;

    public string CookedPizzasCollectionName { get; set; } = null!;
}