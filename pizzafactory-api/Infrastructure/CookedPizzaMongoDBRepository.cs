namespace pizzafactory_api.Infrastructure;

using System.Collections.Generic;
using pizzafactory_api.Domain;
using Microsoft.Extensions.Options;
using MongoDB.Driver;


public class CookedPizzaMongoDBRepository : ICookedPizzaRepository
{
    private readonly IMongoCollection<CookedPizza> _cookedPizzasCollection;

    public CookedPizzaMongoDBRepository(IOptions<PizzaFactoryStoreDatabaseSettings> pizzaFactoryStoreDatabaseSettings)
    {
        var mongoClient = new MongoClient(
            pizzaFactoryStoreDatabaseSettings.Value.ConnectionString);

        var mongoDatabase = mongoClient.GetDatabase(
            pizzaFactoryStoreDatabaseSettings.Value.DatabaseName);

        _cookedPizzasCollection = mongoDatabase.GetCollection<CookedPizza>(
            pizzaFactoryStoreDatabaseSettings.Value.CookedPizzasCollectionName);   
    }

    public async Task<List<CookedPizza>> GetCookedPizzas() =>
        await _cookedPizzasCollection.Find(_ => true).ToListAsync();

    public CookedPizza SaveOneCookedPizza(CookedPizza newCookedPizza){
         _cookedPizzasCollection.InsertOne(newCookedPizza);
         return newCookedPizza;
    }
}
