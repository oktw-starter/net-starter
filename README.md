# pizzafactory-starter

# Documentation

![./pizzafactory-api/docs/assets/clean_onion_architecture.png](./pizzafactory-api/docs/assets/clean_onion_architecture.png)

## Domain

La couche "domain" *(rep : domain)* possède le code invariant métier indépendant de toutes librairies techniques.

### Domain Model

Ensemble des objects métier et des coomportement associée implementés.
Utilisation de design pattenr de type Constructeur, Repository....
Utilisation uniquement d'interface et non de provider technique qui seront injectés par dépendance dans le Constructeur des objets métiers par la couche application. 

### Domain Service

Implémentation de services métiers transversent à plusieurs objets métiers.

### Unit tests

Tests unitaires sur l'ensemble de la couche domain.
Importance de tester l'ensemble de l'implémentaiton des règles métiers.

## Application Service

Services applicatifs *(rep : service)* qui utilisent les objets et service métiers de la couche et se charge d'injecter les provider technique.
De gérer les logs, la config distribuée ou autres provider qui implémentent les interfaces de la couche domaine mise à disposition dans la couche Infrastructure.

## User interface

Interface d'intéraction avec les service applicatifs et les données au travers d'interface web static *(rep : static-ui)* ou même de contoleurs d'api rest *(rep : controller)*

## Infrastructure

Providers d'Infrastructure spécifiques qui impélmentent les interfaces déclarées dans les couches "domaine" et "service".

## Craftmanship guidance

* https://12factor.net/
* https://medium.com/@BranLim/basic-rules-for-effective-onion-architecture-a32af1f3b469 
* https://jeffreypalermo.com/2008/07/the-onion-architecture-part-1/
* https://fr.wikipedia.org/wiki/SOLID_(informatique)

```plantuml
@startuml
class Program {
    - {static} Main(args:string[]) : void
}
class PizzaService {
    + PizzaService(pizzaFactoryStoreDatabaseSettings:IOptions<PizzaFactoryStoreDatabaseSettings>)
    + CookOnePizza(pizza:Pizza) : CookedPizza
    + GetCookedPizzas() : Task<List<CookedPizza>>
}
PizzaService --> "_cookedPizzaMongoDBRepository" ICookedPizzaRepository
class PizzasController {
    + PizzasController(pizzaService:PizzaService)
    + <<async>> Get() : Task<ActionResult<List<CookedPizza>>>
    + Post(pizza:Pizza) : IActionResult
}
ControllerBase <|-- PizzasController
PizzasController --> "_pizzaService" PizzaService
class CookedPizza {
    + reference : string
    + CookedPizza(pizza:Pizza, pizzaiolo:Pizzaiolo)
    + {static} GetCookedPizzas(repository:ICookedPizzaRepository) : Task<List<CookedPizza>>
}
CookedPizza --> "pizza" Pizza
CookedPizza --> "cookDateTime" DateTime
CookedPizza --> "pizzaiolo" Pizzaiolo
interface ICookedPizzaRepository {
    + SaveOneCookedPizza(pizza:CookedPizza) : CookedPizza
    + GetCookedPizzas() : Task<List<CookedPizza>>
}
class Pizza {
    + label : string
    + name : string
    + additionalIngredients : IEnumerable<string>?
    + Pizza(label:string, additionalIngredients:IEnumerable<string>?)
    + Cook(repository:ICookedPizzaRepository) : CookedPizza
}
class Pizzaiolo {
    + matricule : string
    + Pizzaiolo(pizzaioloMatricule:string)
    + ReadCookBook() : SortedList
}
Pizzaiolo --> "cookBook" SortedList
class CookedPizzaMongoDBRepository {
    + CookedPizzaMongoDBRepository(pizzaFactoryStoreDatabaseSettings:IOptions<PizzaFactoryStoreDatabaseSettings>)
    + <<async>> GetCookedPizzas() : Task<List<CookedPizza>>
    + SaveOneCookedPizza(newCookedPizza:CookedPizza) : CookedPizza
}
class "IMongoCollection`1"<T> {
}
ICookedPizzaRepository <|-- CookedPizzaMongoDBRepository
CookedPizzaMongoDBRepository --> "_cookedPizzasCollection<CookedPizza>" "IMongoCollection`1"
class PizzaFactoryStoreDatabaseSettings {
    + ConnectionString : string <<get>> <<set>>
    + DatabaseName : string <<get>> <<set>>
    + CookedPizzasCollectionName : string <<get>> <<set>>
}
class CookedPizzaTest {
}
class PizzaioloTest {
}
class PizzaTest {
}
@enduml
```